const MSGS = require('../messages')
const slugify = require('../functions/slugify')
const AWS = require('aws-sdk');
const fs = require('fs');


module.exports = async function (req, res, next) {
    try {

        const BUCKET_NAME = process.env.S3_BUCKET_NAME 
        console.log('entrou')
        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_ACCESS_KEY_ID ,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
        });

        if (!req.files) {
            console.log('sem arquivo')

            if (req.method == 'PATCH') {
                next()
            } else {
                return res.status(204).send({ error: MSGS.FILE_NOT_SENT });
            }
        } else {
            let photo = req.files.photo
            const name = slugify(photo.name)
            req.body.photo_name = name
            if (photo.mimetype.includes('image/')) {

                const file = await photo.mv(`./uploads/${name}`)

                const params = {
                    Bucket: BUCKET_NAME,
                    ACL: 'public-read',
                    Key: `files/${name}`, // File name you want to save as in S3
                    Body: fs.createReadStream(`./uploads/${name}`)
                };
                s3.upload(params, function (err, data) {
                    if (err) {
                        console.error(err);
                        res.status(500).send(err);
                    } else {
                        console.log(`File uploaded successfully. ${data.Location}`);
                        fs.unlinkSync(`./uploads/${name}`)
                        next()
                    }
                })

            } else {
                return res.status(400).send({ error: MSGS.FILE_INVALID_FORMAT });
            }

        }
    } catch (err) {
        res.status(500).send({ "error": err.message })
    }
}
