const mongoose = require('mongoose');
require('dotenv').config()

const connectDB = async (db) => {
		await mongoose.connect(db, {
			useNewUrlParser: true,
			useCreateIndex: true,
			useFindAndModify: false,
			useUnifiedTopology: true
		});

		console.log('MongoDB Connected...');
	
};

module.exports = connectDB;
