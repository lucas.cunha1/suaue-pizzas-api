module.exports = {
    mongodbMemoryServerOptions: {
      instance: {
        dbName: 'jest'
      },
      binary: {
        version: '4.2.10', // Version of MongoDB -Verifique em seu mongo atlas qual é a versão de seu banco
        skipMD5: true
      },
      autoStart: false
    }
  };
