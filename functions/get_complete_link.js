function get_complete_link(content) {

    const BUCKET_PUBLIC_PATH = process.env.BUCKET_PUBLIC_PATH 
    content.about.photo = `${BUCKET_PUBLIC_PATH}${content.about.photo}`
    content.services.service.map(function(service) {
      service.photo = `${BUCKET_PUBLIC_PATH}${service.photo}`
    })
    content.banner.map(function(banner) {
      banner.photo = `${BUCKET_PUBLIC_PATH}${banner.photo}`
    })
    content.infos.map(function(info) {
      info.icon = `${BUCKET_PUBLIC_PATH}${info.icon}`
    })
    return content
}

module.exports = get_complete_link
