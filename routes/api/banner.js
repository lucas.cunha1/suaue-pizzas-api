const express = require('express');
const Content = require('../../models/home_content');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const auth = require('../../middleware/auth')
const MSGS = require('../../messages')
const file = require('../../middleware/file')
const get_complete_link = require('../../functions/get_complete_link')
const get_max_order = require('../../functions/get_max_order')

router.post('/:contentId', auth, file, async (req, res, next) => {
  try {
    const id = req.params.contentId
    if(req.body.photo_name){
        req.body.photo = `files/${req.body.photo_name}`
      }
    let content = await Content.findOne({ _id : id })
    if (!req.body.order){
      req.body.order = get_max_order(content, 'banner')
      console.log(req.body.order)
    }
    content = await Content.findOneAndUpdate({_id : id}, { $push: { banner: req.body } }, { new: true })
    if (content) {
      content = get_complete_link(content)
      res.json(content)
    } else {
      res.status(404).send({ "error": MSGS.CONTENT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.patch('/:contentId/:bannerId', auth, file, async (req, res, next) => {
  try {
    const id = req.params.contentId
    const bannerId = req.params.bannerId

    let query = {'banner._id' : bannerId}
    if(req.body.photo_name){
      req.body.photo = `files/${req.body.photo_name}`
    }
    let update = {}
    for (const [key, value] of Object.entries(req.body)) {
      update[`banner.$.${key}`] = value
    }
    await Content.updateOne(query, {$set : update}, { new: true })
    let content = await Content.findOne(query)
    
    if (content.id){
      content = get_complete_link(content)
      res.json(content)
    } else {
      res.status(404).send({ "error": MSGS.CONTENT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR  })
  }
})

router.delete('/:contentId', auth, async (req, res, next) => {
  try {
    const id = req.params.contentId
    
    content = await Content.findOneAndUpdate({_id : id}, 
      { $pull: { banner: req.body } }, { new: true })
    if (content) {
      content = get_complete_link(content)
      res.json(content)
    } else {
      res.status(404).send({ "error": MSGS.CONTENT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR  })
  }
})



module.exports = router;
