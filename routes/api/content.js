const get_complete_link = require('../../functions/get_complete_link')
const express = require('express')
const Content = require('../../models/home_content')
const auth = require('../../middleware/auth')
const {  validationResult } = require('express-validator');
const router = express.Router();
const MSGS = require('../../messages')

router.delete('/:contentId', auth, async(req, res, next) => {
  try {
    const id = req.params.contentId
    const content = await Content.findOneAndDelete({_id : id})
    if (content) {
      content = get_complete_link(content)
      res.json(content)
    } else {
      res.status(404).send({ "error": MSGS.CONTENT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.patch('/:contentId', auth, async (request, res, next) => {
  try {
    const errors = validationResult(request)
    if (!errors.isEmpty()) {
      res.status(400).send({ errors: errors.array() })
      return
    }
    const id = request.params.contentId
    const bodyRequest = request.body
    const update = { $set: bodyRequest }
    const content = await Content.findByIdAndUpdate(id, update, { new: true })
    if (content) {
      content = get_complete_link(content)
      res.send(content)
    } else {
      res.status(404).send({ error: MSGS.CONTENT404 })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.get('/', async (req, res, next) => {
  try {
    let content = await Content.findOne({}).sort('-last_modification_date')
    content = get_complete_link(content)
    res.json(content)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.post('/', auth, async (req, res, next) => {
  try {
    let content = new Content(req.body)
    await content.save()
    if (content.id) {
      content = get_complete_link(content)
      return res.json(content);
    }
  } catch (err) {
    console.error(err.message)
    return res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


module.exports = router;
