const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const MSGS = require('../../messages');
const criptografarSenha = require('../../functions/criptografarSenha');

router.post('/login',[
    check('email', MSGS.VALID_EMAIL).isEmail(),
    check('password', MSGS.REQUIRED_PASSWORD).exists()
  ], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }
    const { email, password } = req.body
    try{
        let user = await User.findOne({ email }).select('id password email name')
        if (!user) {
            return res.status(404).json({ errors: [{ msg: MSGS.USER404 }] })
        }else{
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({ errors: [{ msg: MSGS.PASSWORD_INVALID }] });
            }else{
                const payload = {
                    user: {
                      id: user.id,
                      name: user.name
                    }
                }
                jwt.sign( payload, process.env.JWT_SECRET, { expiresIn: '5 days' },
                    (err, token) => {
                      if (err) throw err;
                      payload.token = token
                      res.json(payload);
                    }
                  );
            }
        }

    } catch (err) {
      console.error(err.message)
      res.status(500).send('Server error')
    }

})

router.post('/register',[
  check('email', MSGS.VALID_EMAIL).isEmail(),
  check('password', MSGS.REQUIRED_PASSWORD).exists()
], async (request, response) => {
  try {
      const errors = validationResult(request);
      if (!errors.isEmpty()) {
          return response.status(400).json({ errors: errors.array() })
      }

      let {name,email, password} = request.body
      if(await User.findOne({email: email})){
          return response.status(403).send({errors: [{msg: MSGS.EMAIL_ALREADY_REGISTERED}]})
      }

      password = await criptografarSenha(password)
      const usuario = new User({name,email, password})
      await usuario.save()
      if(usuario.id){
          const payload = {
              user: {
                name: usuario.name,
                  id: usuario.id,
                  email: usuario.email
              }
          }
          jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: '3 days'}, (error, token) => {
              if(error) throw error
              return response.send({token, ...payload})
          })
      }else{
          return response.status(500).send({errors: [{msg: MSGS.DATABASE_ERROR}]})
      }
  } catch (error) {
      console.error(error.message)
      return response.status(500).send({errors: [{msg: MSGS.GENERIC_ERROR}]})
  }
})

module.exports = router;
