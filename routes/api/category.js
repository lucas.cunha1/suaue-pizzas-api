const express = require('express');
const Category = require('../../models/category');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const auth = require('../../middleware/auth')
const MSGS = require('../../messages');
const Product = require('../../models/product');

router.get('/:id', async (req, res, next) => {
    try {
        const id = req.params.id
        const category = await Category.findOne({_id : id})
        if(category){
            res.json(category)
        }else{
            res.status(404).send({ "error": MSGS.CATEGORY404 }) 
        }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
})

router.patch('/:id',auth, async (req, res, next) => {
  try {
      const category = await Category.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
      if(category){
          res.json(category)
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})

router.delete('/:id',auth, async (req, res, next) => {
  try {
    console.log("????????????")
      const id = req.params.id
      let category = await Category.findOne({_id : id})
      if(category){
        console.log("CHECKPOINT 1")
        const productsByCategory = await Product.find({ category : category._id})
        console.log("CHECKPOINT 2")
        if (productsByCategory.length > 0){
          res.status(400).send({ "error": MSGS.CANT_DELETE_THIS_CATEGORY })
        }else{
          console.log("CHECKPOINT 3")
          await Category.findOneAndDelete({_id : id})
          res.json(category)
        }
      }else{
          res.status(404).send({ "error": MSGS.CATEGORY404 }) 
      }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": MSGS.GENERIC_ERROR })
  }
})


router.get('/', async (req, res, next) => {
    try {
      const category = await Category.find({})
      res.json(category)
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })
  
router.post('/',auth, [
    check('name').not().isEmpty(),check('icon').not().isEmpty()
  ], async (req, res, next) => {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
      } else {
        let { name, icon } = req.body
        let category = new Category({ name, icon })
        await category.save()
        if (category.id) {
          res.json(category);
        }
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": MSGS.GENERIC_ERROR })
    }
  })


module.exports = router;
