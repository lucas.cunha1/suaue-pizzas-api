const mongoose = require('mongoose')
const connectDB = require('./config/db')
require('dotenv').config()
const db = process.env.mongoTestURI

module.exports = {
    setupDB () {
      beforeAll(async () => {
        // Connect Database
        connectDB(db)
    });
  
    afterAll(async () => {
        mongoose.connection.close()
    });
    }
  }

