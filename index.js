require('dotenv').config()
const express = require('express')
const cors = require('cors')
const app = express()
const bodyParser = require('body-parser')
const connectDB = require('./config/db')
const fileUpload = require('express-fileupload');
const PORT = process.env.PORT

app.use(fileUpload({
    createParentPath: true
}));
app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api', (request, response) =>{
    response.send('Server is alive')
})

app.use('/api/auth', require('./routes/api/auth'))
app.use('/api/content/banner', require('./routes/api/banner'))
app.use('/api/category', require('./routes/api/category'))
app.use('/api/content', require('./routes/api/content'))
app.use('/api/content/info', require('./routes/api/info'))
app.use('/api/product', require('./routes/api/product'))
app.use('/api/content/service', require('./routes/api/service'))
app.use('/api/user', require('./routes/api/user'))

app.use (function (req, res, next) {
    var schema = (req.headers['x-forwarded-proto'] || '').toLowerCase();
    if (req.headers.host.indexOf('localhost') < 0 && schema !== 'https') {
        res.redirect('https://' + req.headers.host + req.url);
    }
    next();
});

connectDB(process.env.mongoURI)
const server = app.listen(PORT, () => console.log(`Port: ${PORT}`))

module.exports = {app, server}