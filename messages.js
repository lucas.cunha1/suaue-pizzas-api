const MSGS = {
    'CANT_DELETE_THIS_CATEGORY': 'Categoria não pode ser deletada, pois existem produtos associados a ela',
    'CATEGORY404' : 'Categoria não encontrada',
    'CONTENT404' : 'Conteúdo não encontrada',
    "DATABASE_ERROR": "Erro ao gravar informações no banco de dados. Tente novamente mais tarde.",
    'EMAIL_ALREADY_REGISTERED': 'Esse email já foi cadastrado.',
    'FILE_NOT_SENT':'Arquivo não enviado.',
    'GENERIC_ERROR': 'Erro!',
    'INVALID_TOKEN' : 'Token Inválido',
    'PASSWORD_INVALID' : 'Senha incorreta!',
    'PRODUCT404' : 'Produto não encontrado',
    'REQUIRED_PASSWORD' : 'Por favor, insira sua senha.', 
    'USER404' : 'Usuário não encontrado',
    'VALID_EMAIL' : 'Por favor, insira um email válido.',
    'WITHOUT_TOKEN' : 'Token não enviado'
}

module.exports = MSGS
