const mongoose = require('mongoose')
const Content = require('../models/home_content')
const connectDB = require('../config/db')

function setupDB () {
    beforeAll(async () => {
      // Connect Database
      connectDB(process.env.mongoTestURI)
  });

  afterAll(async () => {
      mongoose.connection.close()
  });
  }
  setupDB()

test('should get only one content', async (done) => {
    const content = await Content.find({})
    expect(content).toBeTruthy()
    done()
})