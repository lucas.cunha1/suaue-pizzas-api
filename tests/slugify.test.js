const slugify = require('../functions/slugify')

test('should ', () => {
  expect(slugify('só um teste.jpg')).toBe('so-um-teste.jpg')
})

test('should ', () => {
  expect(slugify('À tarde.scx.jpg')).toBe('a-tarde.scx.jpg')
})
